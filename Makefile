# ----------------------------------------------------------------
# Declaration de variables
PDF = pdflatex
LATEX = latex
VIEW = evince
T = touch
DOC = modelisation
CLEAN = clean
SOURCE = ${DOC}.tex
OUTPUT = ${DOC}.pdf
OUTDVI = ${DOC}.dvi
LOG    = *.log
AUX    = *.aux
OUT    = *.out

# ----------------------------------------------------------------
# Definition explicite des regles

all: dvi pdf clean

dvi :
	$(LATEX) ${SOURCE}
	$(LATEX) ${SOURCE}

pdf :
	$(PDF) ${SOURCE}
	$(PDF) ${SOURCE}


# ----------------------------------------------------------------
#--- utilities
clean:
	rm -f ${OUT} ${LOG} ${AUX}

view:
	${VIEW} ${OUTPUT} &
#-----------------------------------------------------------------
