#!/bin/env python
#
# Author : Eric Parent <eric.parent@kronos.com>

import os.path


class CrossModel(object):

    filename = "cross.lp"

    def __init__(self, n):
        self.N = n
        self.lines = range(1, self.N + 1)
        self.columns = range(1, self.N + 1)
        self.variables = set([])
        self.constraints = []
        self._counter = 1

    def preamble(self, output):
        x_variables = [x for x in set(self.variables) if "K" not in x]
        output.write("Minimize\n")
        output.write(" obj: %s\n" % " + ".join(sorted(x_variables)))
        output.write("\n")
        return output

    def _coeff(self, i, j):
        coeff = "K_%d" % self._counter
        self._counter += 1
        return coeff

    def central_neighborhood(self):
        variables = []
        lines   = range(2, self.N)
        columns = range(2, self.N)
        for i in lines:
            for j in columns:
                _vars = [
                    "X_{%d,%d}" % ( i,  j),
                    "X_{%d,%d}" % (i-1, j),
                    "X_{%d,%d}" % (i+1, j),
                    "X_{%d,%d}" % ( i, j-1),
                    "X_{%d,%d}" % ( i, j+1)
                ]
                coeff = self._coeff(i,j)
                variables.extend(_vars)
                variables.append(coeff)
                line = " CentralNeighborhood_%d%d: %s - 2 %s = 1" \
                       % (i, j, " + ".join(_vars), coeff)
                self.constraints.append(line)
                self.constraint_nb += 1
        self.variables = self.variables | set(variables)

    def lateral_neighborhood(self):
        variables = []
        # left / right cases
        for i in xrange(2, self.N):
            for j in (1, self.N,):
                _vars = [
                    "X_{%d,%d}" % (i-1, j),
                    "X_{%d,%d}" % ( i,  j),
                    "X_{%d,%d}" % (i+1, j),
                ]
                coeff = self._coeff(i, j)
                if j == 1:
                    _vars.append("X_{%d,%d}" % (i, j+1))
                else:
                    _vars.append("X_{%d,%d}" % (i, j-1))
                variables.extend(_vars)
                variables.append(coeff)
                line = " LateralNeighborhood_%d%d: %s - 2 %s = 1" \
                       % (i, j, " + ".join(_vars), coeff)
                self.constraints.append(line)
                self.constraint_nb += 1
        # top / bottom cases
        for j in xrange(2, self.N):
            for i in (1, self.N,):
                _vars = [
                    "X_{%d,%d}" % ( i, j-1),
                    "X_{%d,%d}" % ( i,  j),
                    "X_{%d,%d}" % ( i, j+1)
                ]

                if i == 1:
                    _vars.append("X_{%d,%d}" % (i+1, j))
                else:
                    _vars.append("X_{%d,%d}" % (i-1, j))

                coeff = self._coeff(i, j)
                variables.append(coeff)
                variables.extend(_vars)
                line = " LateralNeighborhood_%d%d: %s - 2 %s = 1" \
                       % (i, j, " + ".join(_vars), coeff)
                self.constraints.append(line)
                self.constraint_nb += 1
        self.variables = self.variables | set(variables)

    def corner_neighborhood(self):
        total = 3
        variables = []
        # left / right cases
        for i in (1, self.N,):
            for j in (1, self.N,):
                _vars = ["X_{%d,%d}" % (i, j)]
                if j == 1:
                    _vars.append("X_{%d,%d}" % (i, j+1))
                    if i == 1:
                        _vars.append("X_{%d,%d}" % (i+1, j))
                    else:
                        _vars.append("X_{%d,%d}" % (i-1, j))
                else:
                    _vars.append("X_{%d,%d}" % (i, j-1))
                    if i == 1:
                        _vars.append("X_{%d,%d}" % (i+1, j))
                    else:
                        _vars.append("X_{%d,%d}" % (i-1, j))
                coeff = self._coeff(i,j)
                variables.append(coeff)
                variables.extend(_vars)
                line = " CornerNeighborhood_%d%d: %s - 2 %s = 1" \
                       % (i, j, " + ".join(_vars), coeff)
                self.constraints.append(line)
                self.constraint_nb += 1
        self.variables = self.variables | set(variables)

    def footer(self, output):
        output.write("\nGeneral\n")
        output.write("\n".join(sorted(self.variables)))
        output.write("\nEnd\n")
        # do work here
        return output

    def write(self):
        self.constraint_nb = 1
        current_dir = os.path.dirname(os.path.abspath(__file__))
        output = open(os.path.join(current_dir, self.filename), "w")

        # gather up the constraints and variables
        self.corner_neighborhood()
        self.lateral_neighborhood()
        self.central_neighborhood()

        # Preamble
        output = self.preamble(output)
        # Constraints
        output.write("Subject To\n")
        output.write("\n".join(self.constraints))
        output.write("\n")

        # Footer
        output = self.footer(output)

        # do work here
        return output


def main():
    N = 3
    model = CrossModel(N)
    model.write()
    return 0

if __name__ == "__main__":
    import sys
    sys.exit(main())
